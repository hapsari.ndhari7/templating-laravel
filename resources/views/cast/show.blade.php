@extends('layouts.master')

@section('title')
    Detail Cast {{$cast->nama}}
@endsection

@section('content')
<h3>{{$cast->nama}}</h3>
<p>
    Umur : {{$cast->umur}} tahun <br>
    Bio : {{$cast->bio}}
</p>
@endsection